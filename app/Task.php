<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'task_description', 'start_date', 'estimated_end_date','user_id'
    ];

        public function users(){
            return $this->belongsToMany('App\User','usertasks');
        }
}
