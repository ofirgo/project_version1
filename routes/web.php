<?php

use Illuminate\Support\Facades\Route;
use App\Http\contorllers\TasksController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

# Tasks Routes
Route::resource('tasks', 'TasksController')->middleware('auth');

Route::get('tasks/delete/{id}', 'TasksController@destroy')->name('tasks.delete');

Route::get('tasks/changeuser/{tid}/{uid?}', 'TasksController@changeUser')->name('task.changeuser');
Route::post('tasks/addUser/', 'TasksController@addUser')->name('task.adduser');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
