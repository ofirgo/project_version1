@extends('layouts.app')

@section('title', 'Create task')

@section('content')
        <h1>Create Task</h1>
        <form method = "post" action = "{{action('TasksController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "task_description">Task Description</label>
            <input type = "text" class="form-control" name = "task_description">
        </div>     
        <div class="form-group">
            <label for = "start_date">Start Date</label>
            <input type = "date" class="form-control" name = "start_date">
        </div>
        <div class="form-group">
            <label for = "estimated_end_date">Estimated End Date</label>
            <input type = "date" class="form-control" name = "estimated_end_date">
        </div> 
        <div>
            <input type = "submit" name = "submit" value = "Create Task">
        </div>                       
        </form>  
@endsection