@extends('layouts.app')

@section('content')
        <div><a href =  "{{url('/tasks/create')}}" > Add New Task</p></a></div>
        <h1>List Of Tasks</h1>
        
        <table class = "table">
            <tr>
                <th>Id</th><th>Description</th><th>Start Date</th><th>End Date</th><th>Owner</th><th>Employees</th><th>Created</th><th>Updated</th><th>Edit</th><th>Delete</th>
            </tr>
            
            @foreach($tasks as $task)
                <tr>
                    <td>{{$task->id}}</td>
                    <td>{{$task->task_description}}</td>
                    <td>{{$task->start_date}}</td>
                    <td>{{$task->estimated_end_date}}</td>
                    <td>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if(isset($task->user_id))
                                  {{$task->users->name}}  
                                @else
                                  Assign owner
                                @endif
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            @foreach($users as $user)
                              <a class="dropdown-item" href="{{route('task.changeuser',[$task->id,$user->id])}}">{{$user->name}}</a>
                            @endforeach
                            </div>
                        </div> 
                                   
                    </td>
                    <td>
                        @foreach(App\User::usersName($task->id) as $user)
                            {{$user->name}}
                        @endforeach  
                    </td>  
                    <td>{{$task->created_at}}</td>
                    <td>{{$task->updated_at}}</td>
                    <td>
                        <a href = "{{route('tasks.edit',$task->id)}}">Edit</a>
                    </td>
                    <td>
                        <a href = "{{route('tasks.delete',$task->id)}}">Delete</a>
                    </td>   
                    <td>
                    <a href = "{{route('tasks.show',$task->id)}}">Add users</a>     
                    </td>
                </tr>
            @endforeach
        </table>
@endsection
