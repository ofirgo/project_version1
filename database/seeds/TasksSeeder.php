<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TasksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert([
            [
            'task_description' => 'Establishment of a garden for Ronit and Zohar',
            'start_date' => date('Y-m-d G:i:s'),
            'estimated_end_date' => date('Y-m-d G:i:s'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            ],

            [
            'task_description' => 'Lawn mowing in the kibbutz',
            'start_date' => date('Y-m-d G:i:s'),
            'estimated_end_date' => date('Y-m-d G:i:s'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            ],

            
        ]);      

    }
}
